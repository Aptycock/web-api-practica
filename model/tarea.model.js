var database = require('../config/database.config');
var tarea = {};

tarea.selectAll = function(callback) {
  if(database) {
    database.query('SELECT * FROM Tarea',
    function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}

tarea.find = function(idTarea, callback) {
  if(database) {
    database.query('call sp_selectTarea(?)', idTarea, function(error, resultado) {
      if(error) throw error;
      if(error) {
				throw error;
			} else {
				callback(resultado);
			}
    })
  }
}

tarea.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_insertTarea(?, ?, ?, now());", [data.idUsuario, data.titulo, data.descripcion],
    function(error, resultado) {
      if(error) throw error;
      callback({"insertId": resultado.insertId});
    });
  }
}



module.exports = tarea;
