var database = require('../config/database.config');
var usuario = {};

usuario.selectAll = function(callback) {
  if(database) {
    database.query('CALL sp_selectUsuario()',
    function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}

usuario.find = function(idUsuario, callback) {
  if(database) {
    database.query('SELECT * FROM Usuario WHERE idUsuario = ?', idUsuario, function(error, resultado) {
      if(error) throw error;
      if(error) {
				throw error;
			} else {
				callback(resultado);
			}
    })
  }
}

usuario.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_insertUsuario(?, ?, ?);", [data.nick, data.contrasena, data.picture],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

usuario.update = function(data, callback) {
  console.log(JSON.stringify(data));
  if(database) {
    var sql = "CALL sp_modifyUsuario(?, ?, ?, ?)";
    database.query(sql,
    [data.idUsuario, data.nick, data.contrasena, data.picture],
    function(error, resultado) {
      if(error) throw error;
      callback(resultado);
    });
  }
}

usuario.delete = function(idUsuario, callback) {
  if(database) {
    var sql = "call sp_deleteUsuario(?)";
    database.query(sql, idUsuario,
    function(error, resultado) {
      if(error) throw error;
      callback({"Mensaje": "Eliminado"});
    });
  }
}



module.exports = usuario;
