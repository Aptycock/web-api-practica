var express = require('express');
var usuarioModel = require('../model/usuario.model');
var usuarioRoute = express.Router();

usuarioRoute.route('/usuario/')
  .get(function(req, res) {
    usuarioModel.selectAll(function(resultados) {
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"mensaje" : "No hay usuarios"});
      }
    });
  })
  .post(function(req, res) {
    var data = {
      nick: req.body.nick,
      contrasena: req.body.contrasena
    }

    usuarioModel.insert(data, function(resultado){
      if(typeof resultado !== undefined && resultado.affectedRows > 0) {
        resultado.status = true;
        resultado.mensaje = "Se registo el usuario correctamente";
        res.json(resultado);
      } else {
        resultado.status = false;
        resultado.mensaje = "Error no se registro el usuario";
        res.json(resultado);
      }
    });
  });

usuarioRoute.route('/usuario/:idUsuario')
  .get(function(req, res) {
    var idUsuario = req.params.idUsuario;
    usuarioModel.find(idUsuario, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se encontro el usuario"})
      }
    });
  })
  .put(function(req, res) {
    var idUsuario = req.params.idUsuario;

    var data = {
      idUsuario: req.body.idUsuario,
      nick: req.body.nick,
      contrasena: req.body.contrasena,
      picture: req.body.picture
    }
      usuarioModel.update(data, function(resultado){
        res.json(resultado);
      });
  })
  .delete(function(req, res) {
    var idUsuario = req.params.idUsuario;
    usuarioModel.delete(idUsuario, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se elimino el usuario"});
      }
    });
  });


module.exports = usuarioRoute;
