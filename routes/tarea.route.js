var express = require('express');
var tareaModel = require('../model/tarea.model');
var tareaRoute = express.Router();

tareaRoute.route('/tarea/')
  .get(function(req, res) {
    tareaModel.selectAll(function(resultados) {
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"mensaje" : "No hay tareas"});
      }
    });
  })
.post(function(req, res) {
      var data = {
        idUsuario: req.body.idUsuario,
        titulo: req.body.titulo,
        descripcion: req.body.descripcion,
        fecha_final: req.body.fecha_final 
      }
      model.insert(data, function(resultado){
      if(resultado.insertId > 0) {
        res.json(data);
      } else {
        res.json({"Mensaje":"No se pudo guardar"});
      }
    });
});

tareaRoute.route('/tarea/:idTarea')
  .get(function(req, res) {
    var idTarea = req.params.idTarea;
    tareaModel.find(idTarea, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se encontro la tarea"})
      }
    });
  })
  .put(function(req, res) {
    var idTarea = req.params.idTarea;

    var data = {
      idTarea: req.body.idTarea,
      nick: req.body.nick,
      contrasena: req.body.contrasena,
      picture: req.body.picture
    }
      tareaModel.update(data, function(resultado){
        res.json(resultado);
      });
  })
  .delete(function(req, res) {
    var idTarea = req.params.idTarea;
    tareaModel.delete(idTarea, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se elimino la tarea"});
      }
    });
  });


module.exports = tareaRoute;